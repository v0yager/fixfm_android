﻿using System;
using Xamarin.Forms;


namespace Fix_FM
{
    public partial class MainPage : ContentPage
    {
        private bool _state;
        public MainPage()
        {
            InitializeComponent();
            _state = false;
        }

        private async void Play_OnClicked(object sender, EventArgs e)
        {
            Button button = (Button) sender;
            button.Image = button.Image == "play.png" ? "pause.png" : "play.png";
            await button.ScaleTo(.5, 500, Easing.SinIn);
            await button.ScaleTo(1, 500, Easing.SinOut);

            _state = _state ? DependencyService.Get<IPlayer>().Stop() : DependencyService.Get<IPlayer>().Play();
        }

        private void Bps128_OnClicked(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            Button button2 = Bps64;
            if (!button.TextColor.Equals(Color.CornflowerBlue)) return;
            button.TextColor=Color.Gold;
            button2.TextColor = Color.CornflowerBlue;
        }

        private void Bps64_OnClicked(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            Button button2 = Bps128;
            if (!button.TextColor.Equals(Color.CornflowerBlue)) return;
            button.TextColor = Color.Gold;
            button2.TextColor = Color.CornflowerBlue;
        }

        private void Dnb_OnClicked(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            Button button2 = Pensia;
            if (!button.TextColor.Equals(Color.CornflowerBlue)) return;
            button.TextColor = Color.Gold;
            button2.TextColor = Color.CornflowerBlue;
        }

        private void Pensia_OnClicked(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            Button button2 = Dnb;
            if (!button.TextColor.Equals(Color.CornflowerBlue)) return;
            button.TextColor = Color.Gold;
            button2.TextColor = Color.CornflowerBlue;
        }
    }
}
