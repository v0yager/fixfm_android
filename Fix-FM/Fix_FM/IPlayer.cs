﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fix_FM
{
    public interface IPlayer
    {
        bool Play();
        bool Stop();
    }
}
