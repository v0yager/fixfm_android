﻿using System;
using Android.App;
using Android.Media;
using Android.Widget;
using Fix_FM.Droid;
[assembly: Xamarin.Forms.Dependency(typeof(PlayerAndroid))]


namespace Fix_FM.Droid
{
    
    public class PlayerAndroid : IPlayer, IDisposable
    {
        private readonly MediaPlayer _player;
        private bool _state;

        public PlayerAndroid()
        {
            _player = new MediaPlayer();
            _player.SetAudioStreamType(Stream.Music);
            _player.SetDataSource("http://fix-fm.net:8008/radio/128");
            _state = false;
        }

        public bool Play() 
        {
            try
            {
                _player.Prepare();
                _player.Start();
                _state = true;
                return _state;
            }
            catch (Exception e)
            {
                Dispose();
                Toast.MakeText(Application.Context, e.Message ,ToastLength.Long).Show();
                return false;
            }
            
        }

        public bool Stop()
        {
            try
            {
                _player.Stop();
                _state = false;
                return _state;
            }
            catch (Exception e)
            {
                Dispose();
                Toast.MakeText(Application.Context, e.Message, ToastLength.Long).Show();
                return false;
            }
        }

        public void Dispose()
        {
            _player.Release();
        }

    }
}